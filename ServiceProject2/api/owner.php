<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app->get('/owner/show', function (Request $request, Response $response, array $args) {
    
    $conn = $GLOBALS['dbconn']; // groblas หาทั้ง project
    $sql = "select * from ownerfolders";
    $result = $conn->query($sql);
    // $num = $result->num_rows;
    $data = array();
    while($row = $result->fetch_assoc()){
        array_push($data,$row);
    }

    $json = json_encode($data);
    $response->getBody()->write($json);

    // $response->getBody()->write("Number rows, $num");
    return $response->withHeader('Content-Type','application/json');
});
$app->get('/owner/show/{foldersID}', function (Request $request, Response $response, array $args) {
    $foldersID = $args['foldersID'];
    $conn = $GLOBALS['dbconn'];
    $stmt = $conn->prepare("select * from ownerfolders where foldersID  = ?");
    $stmt->bind_param("s",$foldersID);
    $stmt->execute();
    $result = $stmt->get_result();
    $data = array();
    while($row = $result ->fetch_assoc()){
        array_push($data,$row);
    }
    $json = json_encode($data);
    $response->getBody()->write($json);
    return $response->withHeader('Content-Type', 'application/json');
});
$app->get('/owner/show2/{userID}', function (Request $request, Response $response, array $args) {
    $userID = $args['userID'];
    $conn = $GLOBALS['dbconn'];
    $stmt = $conn->prepare("select * from ownerfolders where userID  = ?");
    $stmt->bind_param("s",$userID);
    $stmt->execute();
    $result = $stmt->get_result();
    $data = array();
    while($row = $result ->fetch_assoc()){
        array_push($data,$row);
    }
    $json = json_encode($data);
    $response->getBody()->write($json);
    return $response->withHeader('Content-Type', 'application/json');
});
$app->post('/owner/deleteOwner', function (Request $request, Response $response, array $args) {
    $body = $request->getBody();
    $bodyArray = json_decode($body,true);
    $conn = $GLOBALS['dbconn'];
    $IDinDB = foreignkeytabelUsers5($conn,$bodyArray['id']);


    if($bodyArray["id"] == $IDinDB){
            $stmt = $conn->prepare("DELETE FROM ownerfolders "." WHERE userID =?");
            $stmt->bind_param("s",$IDinDB);
            $stmt->execute();
            $result = $stmt->affected_rows;
            $response->getBody()->write($result."");
            return $response->withHeader('Content-Type', 'application/json');
    }
        
    
});
function foreignkeytabelUsers5($conn,$id){
    $stmt = $conn->prepare("SELECT * FROM users where id = ?");
    $stmt->bind_param("s",$id);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($result->num_rows > 0){
        $row = $result->fetch_assoc();
        return $row["id"];
    }else{
        return " ";
    }
}
