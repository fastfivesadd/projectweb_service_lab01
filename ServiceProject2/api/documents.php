<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app->get('/documents/show', function (Request $request, Response $response, array $args) {
    $conn = $GLOBALS['dbconn'];
    $sql = "select * from documents";
    $result = $conn->query($sql);
    $num = $result->num_rows;
    $data = array();
    while($row = $result ->fetch_assoc()){
        array_push($data,$row);
    }
    $json = json_encode($data);
    $response->getBody()->write($json);
    return $response->withHeader('Content-Type', 'application/json');
});

$app->get('/documents/show/{code}', function (Request $request, Response $response, array $args) {
    $pCode = $args['code'];
    $conn = $GLOBALS['dbconn'];
    $stmt = $conn->prepare("select * from documents ".
    "where Titel LIKE '%$pCode%'");
    // $stmt ->bind_param('s',$pCode);
    $stmt->execute();
    $result = $stmt->get_result();
    $data = array();
    while($row = $result ->fetch_assoc()){
        array_push($data,$row);
    }
    $json = json_encode($data);
    $response->getBody()->write($json);
    return $response->withHeader('Content-Type', 'application/json');
});
$app->post('/documents/createDocuments', function (Request $request, Response $response, array $args) {
    date_default_timezone_set('Asia/Bangkok');
    $now = date_create()->format('Y-m-d h:i:s');
    $body = $request->getBody();
    $bodyArray = json_decode($body, true);
    $conn = $GLOBALS['dbconn']; 
    $IDinDB = foreignkeytabelUsers2($conn,$bodyArray['id']);
    $IDfolderNameinDB = foreignkeyTabelfolNameID6666($conn,$bodyArray['nameFolders']);
    $documentID = foreignkeyTabeldoucumentIDTO($conn,$bodyArray['Titel']);
    $response->getBody() ->write("444");
    if ($bodyArray['id'] == $IDinDB){
        $stmt = $conn->prepare("insert into documents"."(foldersID,Titel, NoOfDocuments,dateOFdocument,Destinations,sentTo, referenceTo,CreateDate,CreateBy) "." values (?, ?, ?, ?,?, ?, ?, ?,?)"); 
        $stmt ->bind_param('sssssssss',$IDfolderNameinDB,$bodyArray['Titel'],$bodyArray['NoOfDocuments'],$bodyArray['dateOFdocument'],$bodyArray['Destinations'],$bodyArray['sentTo'],$bodyArray['referenceTo'],$now,$IDinDB);
        $stmt->execute(); 
        $result = $stmt ->affected_rows;
        $response->getBody() ->write($result."");
    }
    return $response->withHeader('Content-Type', 'application/json');
});

$app->post('/document/editDocuments', function (Request $request, Response $response, array $args) {
    date_default_timezone_set('Asia/Bangkok');
    $now = date_create()->format('Y-m-d h:i:s');
    $body = $request->getBody();
    $bodyArray = json_decode($body, true);
    $conn = $GLOBALS['dbconn']; 
    $IDinDB = foreignkeytabelUsers2($conn,$bodyArray['id']);
    $IDfolderNameinDB = foreignkeyTabelfolNameID($conn,$bodyArray['nameFolders']);
    $TitelNameINDB = foreignkeyTabeldocumentsNameID5($conn,$bodyArray['Titel']);
    if ($bodyArray['id'] == $IDinDB && $bodyArray['Titel'] == $TitelNameINDB){
        $stmt = $conn->prepare("UPDATE  documents set ".
        "foldersID=?,Titel=?, NoOfDocuments=?,dateOFdocument=?,Destinations=?,sentTo=?, referenceTo=?,UpdeteDate=?,UpdeteBy=? WHERE foldersID=?"); 

        $stmt ->bind_param('ssssssssss',$IDfolderNameinDB,$bodyArray['Titel'],$bodyArray['NoOfDocuments'],$bodyArray['dateOFdocument'],$bodyArray['Destinations'],$bodyArray['sentTo'],$bodyArray['referenceTo'],$now,$IDinDB,$IDfolderNameinDB);
        $stmt->execute(); 
        $result = $stmt ->affected_rows;
        $response->getBody() ->write($result."");
    } else {
        echo "0 results";
    }
    return $response->withHeader('Content-Type', 'application/json');
});
$app->post('/documents/changeNameDocuments', function (Request $request, Response $response, array $args) {
    date_default_timezone_set('Asia/Bangkok');
    $now = date_create()->format('Y-m-d h:i:s');
    $body = $request->getBody();

    $bodyArray = json_decode($body, true);
    $conn = $GLOBALS['dbconn']; 
    $IDinDB = foreignkeytabelUsers($conn,$bodyArray['id']);
    $TitelINDB = foreignkeyTabelfoldersNameID($conn,$bodyArray['Titel']);
    $ownerfoldersID = foreignkeytabelownerfoldersID($conn,$bodyArray['id']);
    $foldersID = foreignkeyTabelfolderID($conn,$bodyArray['Titel']);
    if ($bodyArray['id'] == $IDinDB && $bodyArray['Titel'] == $nameFolderINDB){
        
        if($ownerfoldersID == $foldersID){
            $stmt = $conn->prepare("UPDATE  folders set ".
            " Titel=?,UpdeteDate=?,UpdeteBy=? WHERE Titel=?"); 
            $stmt ->bind_param('ssss',$bodyArray['newTitel'],$now,$IDinDB,$nameFolderINDB);
            $stmt->execute(); 
            $result = $stmt ->affected_rows;
            $response->getBody() ->write($result);
         }
    } else {
        echo "0 results";
    }
    return $response->withHeader('Content-Type', 'application/json');
});

$app->post('/documents/deleteDocuments', function (Request $request, Response $response, array $args) {
    $body = $request->getBody();
    $bodyArray = json_decode($body,true);
    $conn = $GLOBALS['dbconn'];
    $IDtitelinDB = foreignkeyTabeldocumentsID2($conn,$bodyArray['Titel']);

    if ($IDtitelinDB == $bodyArray['Titel']){
                $stmt = $conn->prepare("DELETE FROM documents "." WHERE Titel=?");
                $stmt->bind_param("s",$IDtitelinDB);
                $stmt->execute();
                $result = $stmt->affected_rows;
                $response->getBody()->write($result."");
                return $response->withHeader('Content-Type', 'application/json');
    }
    
});
function foreignkeyTabeldoucumentIDTO($conn,$Titel){
    $stmt = $conn->prepare("SELECT * FROM documents where Titel = ?");
    $stmt->bind_param("s",$Titel);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($result->num_rows > 0){
        $row = $result->fetch_assoc();
        return $row["id"];
    }else{
        return " ";
    }
}
function foreignkeyTabelfolNameID($conn,$nameFolders){
    $stmt = $conn->prepare("SELECT * FROM folders where nameFolders = ?");
    $stmt->bind_param("s",$nameFolders);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($result->num_rows > 0){
        $row = $result->fetch_assoc();
        return $row["nameFolders"];
    }else{
        return " ";
    }
}
function foreignkeyTabelfolNameID6666($conn,$nameFolders){
    $stmt = $conn->prepare("SELECT * FROM folders where nameFolders = ?");
    $stmt->bind_param("s",$nameFolders);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($result->num_rows > 0){
        $row = $result->fetch_assoc();
        return $row["id"];
    }else{
        return " ";
    }
}
function foreignkeyTabeldocumentsNameID5($conn,$Titel){
    $stmt = $conn->prepare("SELECT * FROM documents where Titel = ?");
    $stmt->bind_param("s",$Titel);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($result->num_rows > 0){
        $row = $result->fetch_assoc();
        return $row["Titel"];
    }else{
        return " ";
    }
}
function foreignkeyTabeldocumentsID2($conn,$Titel){
    $stmt = $conn->prepare("SELECT * FROM documents where Titel = ?");
    $stmt->bind_param("s",$Titel);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($result->num_rows > 0){
        $row = $result->fetch_assoc();
        return $row["id"];
    }else{
        return " ";
    }
}
function foreignkeytabelUsers2($conn,$id){
    $stmt = $conn->prepare("SELECT * FROM users where id = ?");
    $stmt->bind_param("s",$id);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($result->num_rows > 0){
        $row = $result->fetch_assoc();
        return $row["id"];
    }else{
        return " ";
    }
}

   
?>