<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

// $app->get('/customers/{id}', function (Request $request, Response $response, array $args) {
//     $customerID = $args['id'];
//     $response->getBody()->write("Hello Get, $customerID");
//     return $response;
// });
$app->get('/user/show', function (Request $request, Response $response, array $args) {
    
    $conn = $GLOBALS['dbconn']; // groblas หาทั้ง project
    $sql = "select * from users";
    $result = $conn->query($sql);
    // $num = $result->num_rows;
    $data = array();
    while($row = $result->fetch_assoc()){
        array_push($data,$row);
    }

    $json = json_encode($data);
    $response->getBody()->write($json);

    // $response->getBody()->write("Number rows, $num");
    return $response->withHeader('Content-Type','application/json');
});
$app->get('/user/show/{email}', function (Request $request, Response $response, array $args) {
    $email = $args['email'];
    $conn = $GLOBALS['dbconn'];
    $stmt = $conn->prepare("select * from users where email LIKE '%$email%'");
    $stmt->execute();
    $result = $stmt->get_result();
    $data = array();
    while($row = $result ->fetch_assoc()){
        array_push($data,$row);
    }
    $json = json_encode($data);
    $response->getBody()->write($json);
    return $response->withHeader('Content-Type', 'application/json');
});
$app->get('/user/showID/{id}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];
    $conn = $GLOBALS['dbconn'];
    $stmt = $conn->prepare("select * from users where id  = ?");
    $stmt->bind_param("s",$id);
    $stmt->execute();
    $result = $stmt->get_result();
    $data = array();
    while($row = $result ->fetch_assoc()){
        array_push($data,$row);
    }
    $json = json_encode($data);
    $response->getBody()->write($json);
    return $response->withHeader('Content-Type', 'application/json');
});


$app->post('/user/login', function (Request $request, Response $response, array $args) {
    
    $body = $request->getBody();
    $bodyArray = json_decode($body,true);

    $conn = $GLOBALS['dbconn']; 
    $pwdIndb = getPasswordFromDB($conn,$bodyArray['email']);
    if (password_verify($bodyArray['password'],$pwdIndb)){
        // echo "Login Success";
    $stmt = $conn->prepare("select * from users where email =?" );
    $stmt->bind_param("s", $bodyArray['email']);

    $stmt->execute();
    $result = $stmt->get_result();
    $data = array();
    while($row = $result->fetch_assoc()){
        array_push($data,$row);
    }

    $json = json_encode($data);
    $response->getBody()->write($json);

    // $response->getBody()->write("Number rows, $num");
    
    }
    else{
        $json = json_encode("password error");
        $response->getBody()->write($json);
      
    }
    return $response->withHeader('Content-Type','application/json');
});



function getPasswordFromDB($conn,$email){
    $stmt = $conn->prepare("select password from users where email = ?");
    $stmt->bind_param("s",$email);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows == 1){
        $row = $result->fetch_assoc();
        return $row["password"];
    }else{
        return " ";
    }
}

$app->post('/user/editprofile', function (Request $request, Response $response, array $args) {
    $body = $request->getBody();
    $bodyArray = json_decode($body,true);
    $date = GETDATE();
    $conn = $GLOBALS['dbconn']; // groblas หาทั้ง project
    $pwdIndb = getPasswordFromDB($conn,$bodyArray['email']);
    $hashed = password_hash($bodyArray['newpassword'],PASSWORD_DEFAULT); 
    date_default_timezone_set('Asia/Bangkok');
    $now = date_create()->format('Y-m-d h:i:s');
    if (password_verify($bodyArray['password'],$pwdIndb)){
        $stmt = $conn->prepare("update users set password =?,UpdeteDate =?,firstName=?,lastName=? where email=?" );
        $stmt->bind_param("sssss",  $hashed,$now,$bodyArray['firstName'],$bodyArray['lastName'],$bodyArray['email']);
        $stmt->execute();
        $result = $stmt->affected_rows;
        $response->getBody()->write($result."");
        return $response;
    }
    

});
$app->post('/user/register', function (Request $request, Response $response, array $args) {
    date_default_timezone_set('Asia/Bangkok');
    $now = date_create()->format('Y-m-d h:i:s');
    $body = $request->getBody();
    $bodyArray = json_decode($body, true);
    $conn = $GLOBALS['dbconn']; 
    $hashed = password_hash($bodyArray['password'],PASSWORD_DEFAULT); 
    $stmt = $conn->prepare("insert into users"."(email,password,firstName,lastName,CreateDate) "." values (?,?,?,?,?)"); 
    $stmt ->bind_param('sssss', $bodyArray['email'],$hashed, $bodyArray['firstName'], $bodyArray['lastName'],$now);
    $stmt->execute(); 
    $result = $stmt ->affected_rows;
    $response->getBody() ->write($result."");
    return $response->withHeader('Content-Type', 'application/json');
});
