<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app->get('/folders/', function (Request $request, Response $response, array $args) {
    $conn = $GLOBALS['dbconn'];
    $sql = "select * from folders";
    $result = $conn->query($sql);
    $num = $result->num_rows;
    $data = array();
    while($row = $result ->fetch_assoc()){
        array_push($data,$row);
    }
    $json = json_encode($data);
    $response->getBody()->write($json);
    return $response->withHeader('Content-Type', 'application/json');
});

$app->get('/folder/{id}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];
    $conn = $GLOBALS['dbconn'];
    $stmt = $conn->prepare("select * from folders where CreateBy  LIKE '%$id%'");
    // $stmt ->bind_param('s',$pCode);
    $stmt->execute();
    $result = $stmt->get_result();
    $data = array();
    while($row = $result ->fetch_assoc()){
        array_push($data,$row);
    }
    $json = json_encode($data);
    $response->getBody()->write($json);
    return $response->withHeader('Content-Type', 'application/json');
});
$app->get('/folders/show/{id}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];
    $conn = $GLOBALS['dbconn'];
    $stmt = $conn->prepare("select * from folders where id  LIKE '%$id%'");
    // $stmt ->bind_param('s',$pCode);
    $stmt->execute();
    $result = $stmt->get_result();
    $data = array();
    while($row = $result ->fetch_assoc()){
        array_push($data,$row);
    }
    $json = json_encode($data);
    $response->getBody()->write($json);
    return $response->withHeader('Content-Type', 'application/json');
});
$app->post('/folders/createfolders', function (Request $request, Response $response, array $args) {
    date_default_timezone_set('Asia/Bangkok');
    $now = date_create()->format('Y-m-d h:i:s');
    $body = $request->getBody();
    $bodyArray = json_decode($body, true);
    $conn = $GLOBALS['dbconn']; 
    $IDinDB = foreignkeytabelUsers($conn,$bodyArray['id']);
    if ($bodyArray['id'] == $IDinDB){
        $stmt = $conn->prepare("insert into folders"."(nameFolders, CreateDate, CreateBy) "." values ( ?, ?, ?)"); 
        $stmt ->bind_param('sss',$bodyArray['nameFolders'],$now,$IDinDB);
        $stmt->execute(); 
        $result = $stmt ->affected_rows;
        $response->getBody() ->write($result."---");
        $nameFolderINDB = foreignkeyTabelfoldersNameID($conn,$bodyArray['nameFolders']);
        $foldersIDINDB = foreignkeyTabelfoldersID($conn,$bodyArray['nameFolders']);

        if ($bodyArray['nameFolders'] == $nameFolderINDB ){
            $stmt = $conn->prepare("insert into ownerfolders"."(foldersID , userID) "." values (?, ?)"); 
            $stmt ->bind_param('ss', $foldersIDINDB,$IDinDB);
            $stmt->execute(); 
            $result = $stmt ->affected_rows;
            $response->getBody() ->write($result."2");
           
        }

    }
    return $response->withHeader('Content-Type', 'application/json');
});


$app->post('/folders/changeNameFolders', function (Request $request, Response $response, array $args) {
    date_default_timezone_set('Asia/Bangkok');
    $now = date_create()->format('Y-m-d h:i:s');
    $body = $request->getBody();
    $bodyArray = json_decode($body, true);
    $conn = $GLOBALS['dbconn']; 
    $stmt = $conn->prepare("UPDATE  folders set "."nameFolders=?,UpdeteDate=?,UpdeteBy=? WHERE nameFolders=?"); 
    $stmt ->bind_param('ssss',$bodyArray['newnameFolders'],$now,$bodyArray['id'],$bodyArray['nameFolders']);
    $stmt->execute(); 
    $result = $stmt ->affected_rows;
    $response->getBody() ->write($result);
    return $response->withHeader('Content-Type', 'application/json');
});

$app->post('/folders/deleteFolder', function (Request $request, Response $response, array $args) {
    $body = $request->getBody();
    $bodyArray = json_decode($body,true);
    $conn = $GLOBALS['dbconn'];
    $IDfolderinDB = foreignkeyTabelfoldersID($conn,$bodyArray['nameFolders']);
    $stmt = $conn->prepare("SELECT foldersID FROM documents");
    $stmt->execute();
    $result = $stmt->get_result();
    while($row = $result->fetch_assoc()){
        
        if($row["id"]!= $IDfolderinDB){
            $stmt = $conn->prepare("DELETE FROM ownerfolders "." WHERE foldersID =?");
            $stmt->bind_param("s",$IDfolderinDB);
            $stmt->execute();
            $result = $stmt->affected_rows;
            $stmt = $conn->prepare("DELETE FROM folders "." WHERE nameFolders=?");
            $stmt->bind_param("s",$bodyArray['nameFolders']);
            $stmt->execute();
            $result = $stmt->affected_rows;
            $response->getBody()->write($result);   
            return $response->withHeader('Content-Type', 'application/json'); 
        }
    }
    
});

$app->post('/folders/addowner', function (Request $request, Response $response, array $args) {
    
    $body = $request->getBody();
    $bodyArray = json_decode($body, true);
    $conn = $GLOBALS['dbconn']; 
    $IDinDB = foreignkeytabelUsers($conn,$bodyArray['id']);
    $nameFolderINDB = foreignkeyTabelfoldersNameID($conn,$bodyArray['nameFolders']);
    $addOwner = foreignkeytabelUsers($conn,$bodyArray['addOwner']);
    if ($bodyArray['id'] == $IDinDB && $bodyArray['nameFolders'] == $nameFolderINDB && $addOwner == $bodyArray['addOwner']){
        $folderID = foreignkeyTabelfolderID($conn,$bodyArray['nameFolders']);
            $ownerUserID = foreignkeytabelowneruserID($conn,$bodyArray['id']);
            if($ownerUserID == $bodyArray['id']){
                
                $stmt = $conn->prepare("insert into ownerfolders"."(foldersID, userID) "." values (?, ?)"); 
                $stmt ->bind_param('ss', $folderID,$addOwner);
                $stmt->execute(); 
                $result = $stmt ->affected_rows;
                $response->getBody() ->write($result."");
        }
        
    }
    return $response->withHeader('Content-Type', 'application/json');
});


$app->post('/folders/adduser', function (Request $request, Response $response, array $args) {

    $body = $request->getBody();
    $bodyArray = json_decode($body, true);
    $conn = $GLOBALS['dbconn']; 
    $IDinDB = foreignkeytabelUsers($conn,$bodyArray['id']);
    $nameFolderINDB = foreignkeyTabelfoldersNameID($conn,$bodyArray['nameFolders']);
    $adduser = foreignkeytabelUsers($conn,$bodyArray['adduser']);
    if ($bodyArray['id'] == $IDinDB && $bodyArray['nameFolders'] == $nameFolderINDB){
            $ownerUserID = foreignkeytabelowneruserID($conn,$bodyArray['id']);
            if($ownerUserID == $bodyArray['id'] && $adduser != $ownerUserID && $adduser == $bodyArray['adduser']){
              
                $folderID = foreignkeyTabelfoldersID($conn,$bodyArray['nameFolders']);
                $stmt = $conn->prepare("insert into usersfolders"."(foldersID, userID) "." values (?, ?)"); 
                $stmt ->bind_param('ss', $folderID,$adduser);
                $stmt->execute(); 
                $result = $stmt ->affected_rows;
                $response->getBody() ->write($result."");
          
            }else{
                    $response->getBody() ->write("else");
            }
        
    }
    else{
        $response->getBody() ->write("else");
    }
    return $response->withHeader('Content-Type', 'application/json');
});
function foreignkeyTabelfolderID($conn,$nameFolders){
    $stmt = $conn->prepare("SELECT * FROM folders where nameFolders = ?");
    $stmt->bind_param("s",$nameFolders);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($result->num_rows > 0){
        $row = $result->fetch_assoc();
        return $row["id"];
    }else{
        return " ";
    }
}
function foreignkeyTabelfoldersNameID($conn,$nameFolders){
    $stmt = $conn->prepare("SELECT * FROM folders where nameFolders = ?");
    $stmt->bind_param("s",$nameFolders);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($result->num_rows > 0){
        $row = $result->fetch_assoc();
        return $row["nameFolders"];
    }else{
        return " ";
    }
}
function foreignkeyTabelfoldersNameID5555555($conn,$nameFolders){
    $stmt = $conn->prepare("SELECT * FROM folders where nameFolders = ?");
    $stmt->bind_param("s",$nameFolders);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($result->num_rows > 0){
        $row = $result->fetch_assoc();
        return $row["nameFolders"];
    }else{
        return " ";
    }
}

function foreignkeyTabelfoldersID($conn,$nameFolders){
        $stmt = $conn->prepare("SELECT * FROM folders where nameFolders = ?");
        $stmt->bind_param("s",$nameFolders);
        $stmt->execute();
        $result = $stmt->get_result();
        if ($result->num_rows > 0){
            $row = $result->fetch_assoc();
            return $row["id"];
        }else{
            return " ";
        }
    }
function foreignkeytabelUsers($conn,$id){
        $stmt = $conn->prepare("SELECT * FROM users where id = ?");
        $stmt->bind_param("s",$id);
        $stmt->execute();
        $result = $stmt->get_result();
        if ($result->num_rows > 0){
            $row = $result->fetch_assoc();
            return $row["id"];
        }else{
            return " ";
        }
    }
    function foreignkeytabelownerfoldersID($conn,$userID){
        $stmt = $conn->prepare("SELECT * FROM ownerfolders where userID  = ?");
        $stmt->bind_param("s",$userID);
        $stmt->execute();
        $result = $stmt->get_result();
        if ($result->num_rows > 0){
            $row = $result->fetch_assoc();
            return $row["foldersID"];
        }else{
            return " ";
        }
    }
    function foreignkeytabelowneruserID($conn,$userID){
        $stmt = $conn->prepare("SELECT * FROM ownerfolders where userID  = ?");
        $stmt->bind_param("s",$userID);
        $stmt->execute();
        $result = $stmt->get_result();
        if ($result->num_rows > 0){
            $row = $result->fetch_assoc();
            return $row["userID"];
        }else{
            return " ";
        }
    }
?>

