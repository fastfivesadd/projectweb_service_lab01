<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app->get('/usersfolders/show', function (Request $request, Response $response, array $args) {
    
    $conn = $GLOBALS['dbconn']; // groblas หาทั้ง project
    $sql = "select * from usersfolders";
    $result = $conn->query($sql);
    // $num = $result->num_rows;
    $data = array();
    while($row = $result->fetch_assoc()){
        array_push($data,$row);
    }

    $json = json_encode($data);
    $response->getBody()->write($json);

    // $response->getBody()->write("Number rows, $num");
    return $response->withHeader('Content-Type','application/json');
});
$app->get('/usersfolders/show/{foldersID}', function (Request $request, Response $response, array $args) {
    $foldersID  = $args['foldersID'];
    $conn = $GLOBALS['dbconn'];
    $stmt = $conn->prepare("select * from usersfolders where foldersID  = ?");
    $stmt->bind_param("s",$foldersID);
    $stmt->execute();
    $result = $stmt->get_result();
    $data = array();
    while($row = $result ->fetch_assoc()){
        array_push($data,$row);
    }
    $json = json_encode($data);
    $response->getBody()->write($json);
    return $response->withHeader('Content-Type', 'application/json');
});
$app->get('/usersfolders/show2/{userID}', function (Request $request, Response $response, array $args) {
    $userID  = $args['userID'];
    $conn = $GLOBALS['dbconn'];
    $stmt = $conn->prepare("select * from usersfolders where userID  = ?");
    $stmt->bind_param("s",$userID);
    $stmt->execute();
    $result = $stmt->get_result();
    $data = array();
    while($row = $result ->fetch_assoc()){
        array_push($data,$row);
    }
    $json = json_encode($data);
    $response->getBody()->write($json);
    return $response->withHeader('Content-Type', 'application/json');
});
$app->post('/usersfolders/deleteUsers', function (Request $request, Response $response, array $args) {
    $body = $request->getBody();
    $bodyArray = json_decode($body,true);
    $conn = $GLOBALS['dbconn'];
    $IDinDB = foreignkeytabelUsers99($conn,$bodyArray['id']);
    if($bodyArray["id"] == $IDinDB){

            $stmt = $conn->prepare("DELETE FROM usersfolders "." WHERE userID =?");
            $stmt->bind_param("s",$IDinDB);
            $stmt->execute();
            $result = $stmt->affected_rows;
            $response->getBody()->write($result."");
            return $response->withHeader('Content-Type', 'application/json');
    }
        
    
});
function foreignkeytabelUsers99($conn,$id){
    $stmt = $conn->prepare("SELECT * FROM users where id = ?");
    $stmt->bind_param("s",$id);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($result->num_rows > 0){
        $row = $result->fetch_assoc();
        return $row["id"];
    }else{
        return " ";
    }
}
?>