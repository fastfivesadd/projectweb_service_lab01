<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;

require __DIR__ . '/vendor/autoload.php';

$app = AppFactory::create();
$app->setBasePath("/ServiceProject2");

require __DIR__. '/dbconnect.php';
require __DIR__. '/api/customer.php';
require __DIR__. '/api/owner.php';
require __DIR__. '/api/documents.php';
require __DIR__. '/api/folder.php';
require __DIR__. '/api/user.php';


$app->run();